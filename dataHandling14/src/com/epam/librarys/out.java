package com.epam.librarys;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
class out {

    public void outText(){
        int i=0;
        String [] templateKey={"$userName","$номерСчета","$числоМесяцев","$пользовательФИО","$должностьПользователя"};
        String [] templateValue={"Дима","1234567890","8","Ширяев Дмитрий Игоревич","Директор"};
        String text="Уважаемый, $userName, извещаем вас о том, что на вашем счете $номерСчета " +
                "скопилась сумма,\n" +
                " превышающая стоимость $числоМесяцев месяцев пользования нашими услугами. \n" +
                "Деньги продолжают" +
                " поступать. Вероятно, вы неправильно настроили автоплатеж. С уважением,\n " +
                "$пользовательФИО\n" +
                " $должностьПользователя";
        for(String replace:templateKey){
            text = text.replace(templateKey[i],templateValue[i]);
            i++;
        }

        System.out.println(text);
    }
}